#!/usr/bin/env python

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import argparse
import docker
import io
import re
import sys
import yaml


class literal(str):
    '''long string class

    A class to wrap long multi-line strings that should be output in yaml
    using the pipe.
    '''

    pass


def literal_presenter(dumper, data):
    '''Function to output literal in pyyaml'''
    return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')


def setup_yaml_formatter():
    '''Add a formatter for class literal used for long strings'''
    yaml.add_representer(literal, literal_presenter)


def translate_image_name(image_name, registry, tag, exported_registry):
    '''Returns the tuple (source_image, destination_image)

    Translate image name argument to destination_image by adding the
    registry in front of it.
    If image name contains a tag different from latest the returning
    string will contain it.
    Otherwise, it will add <tag> string argument to the image name
    '''
    # A repository name is broken up into path components.
    # A component of a repository name must be at least one lowercase,
    # alpha-numeric characters, optionally separated by
    # periods, dashes or underscores. More strictly, it must match the regular
    # expression [a-z0-9]+(?:[._-][a-z0-9]+)*.
    # If a repository name has two or more path components, they must be
    # separated by a forward slash ("/").
    # The total length of a repository name, including slashes, must be less
    # the 256 characters.
    # Based on this considerations we should be able to simply prefix the old
    # image name with new registry name
    detect_tag_regex = r'(?=[^\/]+$)(?=:(.*))'
    match = re.search(detect_tag_regex, image_name)
    if match:
        source_tag = match.groups()[0]
        source_image = image_name.replace(f':{source_tag}', '')
    else:
        source_tag = 'latest'
        source_image = image_name
    destination_tag = tag
    destination_image = \
        '{}/{}_{}'.format(registry, source_image.replace(':', '_'), source_tag)
    exported_image = \
        '{}/{}_{}'.format(
            exported_registry,
            source_image.replace(':', '_'), source_tag)
    return (
        source_image, source_tag,
        destination_image, destination_tag,
        exported_image)


def valid_image_name(name):
    """Check if a value is a valid container image name

    In practice we exclude image name containing $ signs because they
    represent values that should go through envsubst. The missing value
    is usually the repository supplying the image (NEXUS)
    """
    return '$' not in name


def images_from_k8s_manifests(fd, regex):
    try:
        manifests = yaml.load_all(fd, Loader=yaml.FullLoader)
    except ImportError as e:
        sys.stderr.write("Cannot load yaml file due to error %s\n" % e)
    path = ['spec', 'template', 'spec']
    images = {}
    for manifest in manifests:
        if not manifest:
            continue
        currentdict = manifest
        subpath = []
        for element in path:
            if element in currentdict:
                currentdict = currentdict[element]
                subpath.append(element)
                continue
            else:
                break
        if currentdict:
            for container_type in ['containers', 'initContainers']:
                for container in currentdict.get(container_type, []):
                    image = container.get('image', None)
                    if image is None:
                        cname = container.get('name', '-')
                        mname = manifest.get('metadata', {}).get('name', '-')
                        print(f'Found container without image {cname}'
                              f' in {mname}', file=sys.stderr)
                        continue
                    if valid_image_name(container['image']):
                        images.setdefault(container['image'], [])
                    for variable in container.get('env', []):
                        match = re.search(regex, variable['name'])
                        if match and valid_image_name(variable['value']):
                            ns = manifest['metadata'].get('namespace', None)
                            translation = {
                                'var': variable['name'],
                                'value': variable['value'],
                                'subpath': subpath + [container_type],
                                'container': container['name'],
                                'apiVersion': manifest['apiVersion'],
                                'kind': manifest['kind'],
                                'resource': manifest['metadata']['name'],
                                'namespace': ns
                            }
                            images.setdefault(
                                variable['value'], []).append(translation)
    return images


def images_from_stdin(regex):
    stdin_stream = io.TextIOWrapper(sys.stdin.buffer, encoding='utf-8')
    return images_from_k8s_manifests(stdin_stream, regex)


def sync_images(client, source_image, destination_image):
    '''Sync source_image to destination_image if source_image does not exists

    :param source_image: Source image to synchronize
    :param destination_image: Destination image
    :return: no return
    '''
    try:
        client.images.pull(destination_image)
    except docker.errors.NotFound:
        try:
            image = client.images.pull(source_image)
            tagged = image.tag(destination_image)
            if not tagged:
                sys.stderr.write(
                    f"Encountered error while tagging {source_image} to"
                    f" {destination_image}\n")
                sys.exit(5)
            client.images.push(destination_image)
            client.images.remove(destination_image)
            client.images.remove(source_image)
        except docker.errors.APIError as e:
            sys.stderr.write(
                f"Encountered error {e} while synchronizing"
                f"image {source_image} to {destination_image}\n")
            sys.exit(1)
    except docker.errors.APIError as e:
        sys.stderr.write(
            f"Encountered error {e} while trying to look after"
            f"image {destination_image}\n")
        sys.exit(1)


def variable_patch(translation, new_name):
    """Compute the patch to modify a variable

    Variables can contain reference to image, this code computes the
    JSON patch from the information obtained during the exploration phase.
    """
    patch = [{
        'name': translation['container'],
        'env': [{'name': translation['var'], 'value': new_name}]
    }]
    subpath = translation['subpath']
    subpath.reverse()
    for key in subpath:
        patch = {key: patch}
    patch['kind'] = translation['kind']
    patch['apiVersion'] = translation['apiVersion']
    patch['metadata'] = {'name': translation['resource']}
    ns = translation['namespace']
    if ns is not None:
        patch['metadata']['namespace'] = ns
    text = yaml.safe_dump(patch)
    return literal(text)


def main():
    parser = argparse.ArgumentParser(description='Image sync tool for kanod')
    parser.add_argument(
        '--registry', required=True,
        help='Destination registry for container images')
    parser.add_argument(
        '-d', '--debug', action='store_true',
        help='Destination registry for container images')
    parser.add_argument(
        '--parse-stdin', action='store_true', default=False,
        help='Whether to parse stdin for k8s manifests (default false)')
    parser.add_argument(
        '--export', action='store_true', default=False,
        help='Whether to export kustomization for images')
    parser.add_argument(
        '--exported-registry',
        help='Destination registry written in exports for container images')
    parser.add_argument(
        '--tag',
        help='Tag added to all images stored in destination registry')
    parser.add_argument(
        '--user', default=None,
        help='User to authenticate against registry')
    parser.add_argument(
        '--password', default=None,
        help='Password used to authenticate against registry')
    parser.add_argument(
        '-e', '--envvar-image-regexp', default='.*_IMAGE',
        dest='envvar_image_regexp',
        help=('Regular expression to use when looking after container '
              'environment variables containing image names'))
    options = parser.parse_args()
    debug = options.debug
    export = options.export
    exported_registry = (
        options.registry if options.exported_registry is None
        else options.exported_registry)
    tag = options.tag
    parse_stdin = options.parse_stdin
    username = options.user
    password = options.password
    destination_registry = options.registry
    var_image_regexp = options.envvar_image_regexp
    if debug and export:
        parser.error(
            "You should not use debug and export together "
            "since debug is adding unwanted output to stdout")
    if parse_stdin and not tag:
        parser.error(
            "If you run the program with manifests input you have to "
            "give it a tag")
    if not parse_stdin:
        parser.error(
            "You have to give the utility at least one of: --parse-stdin "
            "or ... arguments")
    try:
        client = docker.DockerClient(base_url='unix://var/run/docker.sock')
    except FileNotFoundError as e:
        sys.stderr.write(
            f"Encountered error while connecting to docker: {e}\n")
        sys.exit(3)
    if username is not None:
        login_result = client.login(
            username=username, password=password,
            registry=destination_registry, reauth=True)
        if not login_result.get('Status', None):
            sys.stderr.write(
                f"Failed login to the docker registry\n"
                f"Login result was: {login_result}\n")
            sys.exit(4)
    images = (images_from_stdin(var_image_regexp) if parse_stdin else {})
    # Sync images from manifests
    if debug:
        print("Found source images: {}".format(" ".join(images.keys())))
    images_dictionary = []
    sys.stderr.write("Starting to synchronize images: ")
    warnings = []
    patches = []
    setup_yaml_formatter()

    for (image, translations) in images.items():
        if destination_registry in image:
            warnings.append(
                f"Excluding image {image} since it seems"
                f" to refer to destination registry")
            continue
        source_image, source_tag, dest_image, dest_tag, exported_image = \
            translate_image_name(
                image, destination_registry, tag, exported_registry)
        image_dictionary = {
            'name': source_image,
            'newName': exported_image,
            'newTag': dest_tag,
        }
        images_dictionary.append(image_dictionary)

        new_name = f'{exported_image}:{dest_tag}'
        for translation in translations:
            patches.append(variable_patch(translation, new_name))
        sync_images(
            client,
            f'{source_image}:{source_tag}',
            f'{dest_image}:{dest_tag}')
        sys.stderr.write("#")
    sys.stderr.write("\nFinalized image synchronization\n")
    sys.stderr.write('Warnings:\n\t{}'.format("\n\t".join(warnings)))
    if export:
        sys.stdout.write(yaml.dump({
            'images': images_dictionary,
            'patchesStrategicMerge': patches
        }))


if __name__ == '__main__':
    main()
