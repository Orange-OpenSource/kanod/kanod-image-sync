# Tool for synchronising kanod docker images from upstream

## Description
The tool is intended to be run in a gitlab pipeline since it is expecting to have access to kanod-stack code.

The tool will try to get all the images obtained by running:

`kubeadm config images list` and by parsing the standard input or the output of `kubectl kustomize .`.

and excluding images that point to the destination registry.

## How to run it

There are several ways of using the sync image script, depending whether you want to get the information from standard input or from kubectl kustomize.

```shell script
usage: sync-images.py [-h] --registry REGISTRY [--debug] [--kustomize] [--parse-stdin] [--kubeadm] [--export] [--tag TAG] --user USER --password PASSWORD [--kubeadm-path KUBEADM_PATH]

Image sync tool for kanod

optional arguments:
  -h, --help            show this help message and exit
  --registry REGISTRY   Destination registry for container images
  --debug               Destination registry for container images
  --kustomize           Whether to run "kubectl kustomize ." to get k8s manifests (default false)
  --parse-stdin         Whether to parse stdin for k8s manifests (default false)
  --kubeadm             Whether to run kubeadm to get the k8s components container images (default false)
  --export              Whether to export kustomization for images
  --tag TAG             Tag added to all images stored in destination registry
  --user USER           User to authenticate against registry
  --password PASSWORD   Password used to authenticate against registry
  --kubeadm-path KUBEADM_PATH
                        Full path to kubeadm binary
``` 

The image built using the included Dockerfile have to be started in a docker gitlab runner with a command similar with:

```
docker run -it -v /var/run/docker.sock:/var/run/docker.sock image-sync bash
```

The runner need to map the docker socket in /var/run/docker.sock in the container.

One way to run the script is: 
```shell script
cat stack/*.yml | envsubst | sync-images.py --parse-stdin --tag 1.2.2 --registry localhost --export
```
If the image does not have a tag specified, the tool will sync the latest tag.

There is only one exception: for kubeadm images we do not change the tags, we are only updating the registry name.
