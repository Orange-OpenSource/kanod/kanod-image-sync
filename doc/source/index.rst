================
Kanod Image Sync
================

Overview
========

Kanod Image Sync is a Tool for synchronising kanod docker images from upstream.
It serves two purposes:

* Upload to a container registry all the container images used by a manifest.
  It is similar to the effect of using the registry as a cache but without
  explicitly running the containers.
* Replace ``master`` tags with unique numbers so that we have a reference
  to container versions that we can keep in time.

Most deployments are in air-gapped environments where we do not have access to
the internet. kanod-image-sync can be used to prepare Nexus images that can
be deployed in those environments.

The tool is usually run in a gitlab pipeline since it is expecting to have
access to kanod-stack code.

The tool will try to get all the images obtained by running:

``kubeadm config images list`` and by parsing the standard input or the output
of ``kubectl kustomize .``.

and excluding images that point to the destination registry.

Usage
=====

There are several ways of using the sync image script, depending whether you want to get the information from standard input or from kubectl kustomize.

shell script
------------

.. code-block:: bash

    kanod-image-sync [-h] --registry REGISTRY [--debug] [--kustomize]
        [--parse-stdin] [--kubeadm] [--export] [--tag TAG] --user USER
        --password PASSWORD [--kubeadm-path KUBEADM_PATH]

.. option:: -h, --help

    show this help message and exit

.. option:: --registry REGISTRY

    Destination registry for container images

.. option:: --debug

    Destination registry for container images

.. option:: --kustomize

    Whether to run "kubectl kustomize ." to get k8s manifests (default false)

.. option:: --parse-stdin

    Whether to parse stdin for k8s manifests (default false)

.. option:: --kubeadm

    Whether to run kubeadm to get the k8s components container images (default false)

.. option:: --export

    Whether to export kustomization for images

.. option:: --tag TAG

    Tag added to all images stored in destination registry

.. option:: --user USER

    User to authenticate against registry

.. option:: --password PASSWORD

    Password used to authenticate against registry

.. option:: --kubeadm-path KUBEADM_PATH

    Full path to kubeadm binary


The image built using the included Dockerfile have to be started in a docker
gitlab runner with a command similar with:

.. code-block:: bash

    docker run -it -v /var/run/docker.sock:/var/run/docker.sock kanod-image-sync

The runner need to map the docker socket in /var/run/docker.sock in the container.

The companion project ``kanod-interalize`` provides a simple canvas for
specializing external manifests like calico. The only variables that must
be defined are:

    ``URL``
        The URL of the manifest to specialize

    ``VERSION``
        The tag that will be associated to the generated containers

    ``ARTIFACT``
        The name of the artifact containing the modified manifest
        in the Maven repository

    ``REPO_URL``
        Access to the Maven repository

    ``NEXUS_REGISTRY``
        Name of the Docker registry

    ``NEXUS_ADMIN_USER``
        Name of the Nexus administrator

    ``NEXUS_ADMIN_PASSWORD``
        Password of the Nexus administrator
